#### Split Setup Manual
## **N.B.**: Prior to installing the Split SDK, ensure that you have access to a [Split Account](https://www.split.io/signup/) to enable feature flagging.

## Installing the SDK
In your terminal, execute the following command to add the Split SDK to your project: ``npm install @splitsoftware/splitio``

## Modifying your code
Create a new file in src/app called splitio.service.ts to act as an abstraction for Split:
``ng generate service splitio``

Open the file and insert the following code into it:
```
import { Injectable } from '@angular/core';
import { SplitFactory } from '@splitsoftware/splitio';
import { fromEvent } from 'rxjs';

@Injectable()
export class SplitioService {

  splitio: SplitIO.ISDK;
  splitClient: SplitIO.IClient;
  isReady = false;
  treatments: SplitIO.Treatments;
  features: string[] = [    'feature_1',    'feature_2',    'feature_3'  ];

  constructor() { }

  initSdk(): void {
    this.splitio = SplitFactory({
      core: {
        authorizationKey: '<YOUR-API-KEY>',
        Key: 'ANONYMOUS_USER'
      },
      // In non-localhost mode, this map is ignored.
      features: {
        feature_1: 'off',
        feature_2: 'on',
        feature_3: 'v2'
      }
    });

    this.splitClient = this.splitio.client();

    // verify if sdk is initialized
    this.verifyReady();
  }

  private verifyReady(): void {
    const isReadyEvent = fromEvent(this.splitClient, this.splitClient.Event.SDK_READY);

    const subscription = isReadyEvent.subscribe({
      next() {
        this.isReady = true;
        console.log('Sdk ready: ', this.isReady);
      },
      error(err) {
        console.log('Sdk error: ', err);
        this.isReady = false;
      }
    });
  }

  isTreatmentOn(treatmentName: string) : boolean {
    let treatment = this.splitClient.getTreatment(treatmentName);
    let result = null;

    if (treatment === 'on') {
      result = true;
    } else if (treatment === 'off') {
      result = false;
    } else {
      throw new Error('Unable to reach Split.' + treatment);
    }

    return result;
  }
}
```


Now, change the code of the component with the features you intend to flag.

Firstly, add the following argument into the constructor: ``constructor( <arg1>, <arg2>, ... , private splitioService: SplitioService)``

Secondly, add the following line to the `ngOnInit` method: ``this.splitioService.initSdk();``

Thirdly, change the target method to implement the Split treatment types. For example, if you are modifying the search results of a component, find the method that performs the actual search then employ an `if-else` block (we can use a `switch statement` as well) to modify the search results according to the treatment set in your Split account :
```
  // Get the treatment result
  let treatment = this.splitioService.getTreatment('')

  if(result = 'on'){
    // Enter treatment code here.
  }
  else if(result = 'off'){
    // Enter treatment code here.
  }

```

To conclude, we will need to tell Angular to inject an instance of the Split service into the Search component. Open the `src/app/app.module.ts` file and add the following import to it:
``import { SplitioService } from './splitio.service'``

Next, scroll to the bottom of the file and locate the line containing the `providers: []` property and add the following line into the array as follows: ``providers:[SplitioService],`` 

## Accessing the Split Account
Once you are signed in to your Split Account, navigate to the dashboard and select the "DE" button in the upper-left corner. This is the workspace button.

Click on "Admin settings", then on "API Keys" on the left panel. This diplays the keys for all of your environments.

## Create a Split
To create an actual treatment with Split, go back to the Split dashboard and click "Create Split," assign it a name and click "Create."

**N.B.**: make sure the name of your split matches the name entered in the actual code.

Next, click on "Add Rules" and, if you wish, add more treatments under "Define treatments." In our case: we've added a `v2` treatment to match the features property in our split SDK. You will notice that the default treatment is `off` to prevent you from unintentionally activing a feature before it is ready

Click on "Save Changes," and finally "Confirm." .

Return to the "API Keys" panel under DE > Admin Settings and search for the API you just created. Once you have located the row, click "Copy" to the right of the API key to store it in your clipboard.

Now, return to the splitio.service.ts file and within the  the initSDK and paste the API key to the `authorizationKey' placeholder value.