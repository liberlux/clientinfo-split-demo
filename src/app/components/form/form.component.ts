import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Key } from 'protractor';
import { Client } from 'src/app/interfaces/client';
import { ClientService } from 'src/app/services/client.service';
import { SplitioService } from 'src/app/services/splitio.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  selClientId;
  clientForm;
  clients: Client[];
  private result;
  constructor(private formBuilder: FormBuilder,
              private clientService: ClientService,
              private splitioService: SplitioService) {
    this.clientForm = formBuilder.group({
      id: null,
      name: '',
      contactEmail: '',
      noReplyEmail: '',
      servicePhoneNumber: '',
      lostOrStolenPhoneNumber: '',
    })
  }

  /**
   * Initialization of the form
   */
  ngOnInit() {
    this.splitioService.initSDK();
    this.selClientId = null;
    this.clientService.getClients().subscribe((clients) => {
      this.clients = clients as Client[];
    });
  }

  getClientById(clientId: number): Client {
    let selClient: Client;
    for (let i = 0; i < this.clients.length; i++) {
      if (this.clients[i].id == clientId) {
        selClient = this.clients[i];
        return selClient;
      }
    }
  }

  /**
   * Updates the value of the form fields to that of the Client
   * @param id 
   * @returns {{void}}
   */
  onChange(id) {
    let newClient = this.getClientById(id);
    this.result = this.splitioService.getTreatment('clientInfo-split-demo');

    const keys = Object.keys(newClient);

    switch (this.result) {
      case 'v1':
        keys.forEach((key) => {
          if(newClient[key] == null){
            newClient[key] = 'N/A';
          }
        })
        break;
      case 'off':
        break;
      case 'v2':
        keys.forEach((key) => {
          if(key.includes('Email') && newClient[key] == null){
            newClient[key] = 'custom@pscu.com';
          }
          if(key.includes('PhoneNumber') && newClient[key] == null){
            newClient[key] = '555-555-555';
          }
        })
        break;
    }

    this.clientForm.setValue({
      id: newClient.id,
      name: newClient.name,
      contactEmail: newClient.contactEmail,
      noReplyEmail: newClient.noReplyEmail,
      servicePhoneNumber: newClient.servicePhoneNumber,
      lostOrStolenPhoneNumber: newClient.lostOrStolenPhoneNumber
    })
  }

  /**
   * Updates the value of the Form.
   * @param newClient
   */
  onSubmit(newClient) {
    console.log('newClient', JSON.stringify(newClient))
    for (let i = 0; i < this.clients.length; i++)
      if (this.clients[i].id == newClient.id) {
        this.clients[i].contactEmail = newClient.contactEmail;
        this.clients[i].noReplyEmail = newClient.noReplyEmail;
        this.clients[i].servicePhoneNumber = newClient.servicePhoneNumber;
        this.clients[i].lostOrStolenPhoneNumber = newClient.lostOrStolenPhoneNumber;
      }

    this.clientForm.reset();
    this.selClientId = null;
    
    this.result = this.splitioService.getTreatment('clientInfo-split-demo');
    if(this.result == 'v3'){
      let target = document.getElementById('reset');
      target.style.visibility = 'hidden';
    }

  }
}