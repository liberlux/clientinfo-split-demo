import { Injectable } from '@angular/core';
import { SplitFactory } from '@splitsoftware/splitio';
import * as SplitIO from '@splitsoftware/splitio/types/splitio';
import { fromEvent } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SplitioService {
  splitio: SplitIO.ISDK;
  splitClient: SplitIO.IClient;
  isReady = false;
  treatments: SplitIO.Treatments;
  features: string[] = [ 'feature_1', 'feature_2', 'feature_3']
  
  constructor() { }

  initSDK(): void {
    this.splitio = SplitFactory({
      core: {
        authorizationKey: 'ei2gu6m1gsmgj9ia8d06692isrrm79m71i00',
        key: 'ANONYMOUS_USER'
      },
      features: {
        feature_1: 'off',
        feature_2: 'on',
        feature_3: 'v2'
      }
    });

    // Get a client instance to use from the split sdk
    this.splitClient = this.splitio.client();

    //verify if sdk initialized
    this.verifyReady();
  }

  private verifyReady(): void {
    const isReadyEvent = fromEvent(this.splitClient, this.splitClient.Event.SDK_READY);

    const subscription = isReadyEvent.subscribe({
      next() {
        this.isReady = true;
        console.log('Sdk ready: ', this.isReady)
      },
      error(err){
        console.log('Sdk error: ', err);
        this.isReady = false;
      }
    });
  }

  getTreatment(treatmentName :string) : string{
    var treatment: string;
    treatment = this.splitClient.getTreatment(treatmentName);
    return treatment;
  }
}