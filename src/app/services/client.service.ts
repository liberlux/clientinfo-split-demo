import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Client } from '../interfaces/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  clients: Client[] =  [
    {
      id: 225735,
      name: 'Forward One',
      contactEmail: 'client@pscu.com',
      servicePhoneNumber: null,
      noReplyEmail: null,
      lostOrStolenPhoneNumber: null
    },
    {
      id: 2851,
      name: 'Atlantic',
      contactEmail: 'test@pscu.com',
      noReplyEmail: 'test@pscu.com',
      servicePhoneNumber: '555-453-5445',
      lostOrStolenPhoneNumber:'555-453-5445'
    },
    {
      id: 2131,
      name: 'Advantage FCU',
      contactEmail: 'memberinfo@advantagefcu.org',
      noReplyEmail: 'memberinfo@advantagefcu.org',
      servicePhoneNumber: '585-454-5900',
      lostOrStolenPhoneNumber: '866-572-4141'
    },
    {
      id: 2718,
      name: 'FivePoint',
      contactEmail: 'info@pscu.com',
      noReplyEmail: 'noreply@pscu.com',
      servicePhoneNumber: '111-111-111',
      lostOrStolenPhoneNumber: '111-111-1111'
    },
    {
      id: 6379,
      name: 'York County',
      contactEmail: null,
      noReplyEmail: null,
      servicePhoneNumber: '131-324-3434',
      lostOrStolenPhoneNumber: '180-044-9772'
    }
  ]

  getClients(): Observable<any> {
    return of(this.clients);
  }
  
  constructor() { }
}