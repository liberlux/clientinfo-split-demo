export interface Client {
    id: Number;
    name: String;
    contactEmail?: String;
    noReplyEmail?:String;
    servicePhoneNumber?: String;
    lostOrStolenPhoneNumber: String
}
