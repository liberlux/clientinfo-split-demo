### Split overview
Implementing feature toggling requires the following:
1. Create a new service to act as abstraction for Split
2. Modifying the code of the component
3. Within said component, modifying the method that will be returning the set of data of interest so that it checks in with the Split service class to verify whether the treatment (that is, the feature flag) is active before returning the results.
4. Instructing Angular to inject an instance of SplitService into the `Search` component.

## The Split service
The `splitio.service` class acts as an abstraction for a Split interface. It imports from two namespaces:
* `@splitsoftware/splitio/types/splitio'
* `@splitsoftware/splitio`

From these software packages we will add three properties in addition to a boolean and string array.
* An instance of the Split `ISDK` class 
* An instance of the Split `IClient` interface 
* A set of treatments

The `ISDK` object is responsible for generating the API requests used to communicate with the Split server. It is built with the `initSDK()` method through use of the static method `SplitFactory()` that configures its principal properties: namely `core`, which contains the actual Split API key, and `features` which stores the treatments for feature flagging.

The `IClient` interface gets a client instance to use from the split sdk and listen for any events from the Split server.

Within the `initSDK()` method, however, we need to first verify that the SDK has been initialized. Hence the need for the class boolean property: `isReady`.

Once the SDK is instantiated, we create a `verifyReady()` to verify that the SDK method is ready for use. The method itself uses the `fromEvent()` method with our Split Client to listen specifically for the `SDK_READY` event. Once it has received said event, the `isReady` variable is set to true indicating that the SDK is ready for use.

Lastly, the splitio service will use a custom `getTreatment()` method to receive and return the treatment sent out by the SDK server. The method itself takes in a string representing the name of the treatment; this treatment name correspond to the name of the Split we created in our Split account and therefore both of which must be identical to each other.
